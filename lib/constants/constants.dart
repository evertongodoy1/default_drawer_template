class Constants {
  // AppBar Titles
  static const String HOME_TITLE_APPBAR = "Default HOME";
  static const String ITEM00_TITLE_APPBAR = "Default Item 00";
  static const String ITEM01_TITLE_APPBAR = "Default Item 01";
  static const String AGENDA_TITLE_APPBAR = "Agenda";

  // Drawer
  static const String DRAWERHEADER = 'Default Drawer Header';
}
