import 'package:flutter/material.dart';

import '../widgets/custom_appbar.dart';
import '../constants/constants.dart';
import '../widgets/custom_drawer.dart';

class Item01Screen extends StatelessWidget {
  static const route = '/item01';

  @override
  Widget build(BuildContext context) {
    // Caso queira recuperar um valor da tela que chamou
    var previousPageValue = ModalRoute.of(context).settings.arguments;
    print(previousPageValue);

    return Scaffold(
      appBar: AppBarCustomizada(
        appBarTitle: Constants.ITEM01_TITLE_APPBAR,
      ),
      //drawer: CustomDrawer(),
      body: Align(
        alignment: Alignment.center,
        child: Text('Default text Item 00 Page $previousPageValue'),
      ),
    );
  }
}
