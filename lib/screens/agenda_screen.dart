import 'package:flutter/material.dart';

import '../widgets/custom_appbar.dart';
import '../constants/constants.dart';

class AgendaScreen extends StatelessWidget {
  static const route = '/agenda';

  @override
  Widget build(BuildContext context) {
    // Caso queira recuperar um valor da tela que chamou
    Map previousPageValue = ModalRoute.of(context).settings.arguments;
    print(previousPageValue);

    previousPageValue['Uid'] = 'U1oo1aaaaaa';
    print(previousPageValue);

    print(previousPageValue['chave2']);
    String textBody = previousPageValue['Uid'];

    return Scaffold(
      appBar: AppBarCustomizada(
        appBarTitle: Constants.AGENDA_TITLE_APPBAR,
      ),
      body: Align(
        alignment: Alignment.center,
        child: Text('Default text Agenda Page $textBody'),
      ),
    );
  }
}
