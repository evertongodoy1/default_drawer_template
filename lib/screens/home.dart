import 'package:default_drawer_template/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';

import '../widgets/custom_appbar.dart';
import '../constants/constants.dart';
import '../widgets/custom_listtile.dart';

class Home extends StatelessWidget {
  static const route = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarCustomizada(
        appBarTitle: Constants.HOME_TITLE_APPBAR,
      ),
      body: Align(
        alignment: Alignment.center,
        child: Text('Default text body'),
      ),
      drawer: CustomDrawer(),
    );
  }
}
