import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  final String listTileTitle;
  final Icon iconType;
  final String finalRouteNavigate;
  final Object args;

  CustomListTile(
      {this.listTileTitle, this.iconType, this.finalRouteNavigate, this.args});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: iconType,
      title: Text(listTileTitle),
      onTap: () {
        // Update the state of the app
        // Then close the drawer
        Navigator.pop(context);

        // Navigator depende dos vaores contidos na variavel Context
        Navigator.of(context).pushNamed(
          finalRouteNavigate,
          arguments: args, // Send argumentos
        );
      },
    );
  }
}
