import 'package:flutter/material.dart';

import './custom_listtile.dart';
import '../constants/constants.dart';

// Pages
import '../screens/item00_screen.dart';
import '../screens/item01_screen.dart';
import '../screens/agenda_screen.dart';

// https://stackoverflow.com/questions/57237072/how-to-make-a-flutter-drawerheader-fixed

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      //elevation: 20.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(
            //height: 120,
            child: DrawerHeader(
                child: Text('NOSSO HEADER'),
                decoration: BoxDecoration(color: Colors.blue),
                margin: EdgeInsets.only(top: 0)),
          ),
          Expanded(
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                //
                //CustomListTile(
                //  listTileTitle: 'Home',
                //  iconType: Icon(Icons.home),
                //  finalRouteNavigate: Home.route,
                //),
                CustomListTile(
                  listTileTitle: 'Item 00',
                  iconType: Icon(Icons.adb),
                  finalRouteNavigate: Item00Screen.route,
                  args: 'test 0', //{'chave': 'valor 000000'},
                ),
                CustomListTile(
                  listTileTitle: 'Item 01',
                  iconType: Icon(Icons.person),
                  finalRouteNavigate: Item01Screen.route,
                  args: 'test 1', // {'chave': 'valor 11111'},
                ),
                CustomListTile(
                  listTileTitle: 'Agenda',
                  iconType: Icon(Icons.schedule),
                  finalRouteNavigate: AgendaScreen.route,
                  args: {
                    'chave': 'valor 11111',
                    'chave2': 'valor 22222'
                  }, // {'chave': 'valor 11111'},
                ),
                //
              ],
            ),
          )
        ],
      ),
    );
  }
}
