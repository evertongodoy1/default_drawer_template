import 'package:flutter/material.dart';

class AppBarCustomizada extends StatelessWidget with PreferredSizeWidget {
  final String appBarTitle;

  AppBarCustomizada({this.appBarTitle});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(appBarTitle),
      centerTitle: true,
    );
  }

  @override
  // kToolbarHeight = E uma variavel que determina o padrao da altura da AppBar
  // Por padrao o valor e igual a 5 pixels
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
