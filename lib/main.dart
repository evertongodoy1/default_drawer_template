import 'package:flutter/material.dart';

import './screens/home.dart';
import './screens/item00_screen.dart';
import './screens/item01_screen.dart';
import './screens/agenda_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        //home: Home(), - Usando a rota Home abaixo, nao pode duplicar aqui

        // Rotas - Recebe um Map
        // initialRoute: '', Opcional
        routes: {
          Home.route: (_) => Home(),
          Item00Screen.route: (_) => Item00Screen(),
          Item01Screen.route: (_) => Item01Screen(),
          AgendaScreen.route: (_) => AgendaScreen(),
        });
  }
}
